#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# Basic settings
AUTHOR = "Andraz Brodnik brodul"
SITENAME = "Andraž Brodnik brodul"
SITEURL = "http://localhost:8000"
ALCHEMY_META_DESCRIPTION = "brodul blog Andraz Brodnik Programmer DevOps"
THEME_CSS_OVERRIDES = ["theme/css/oldstyle.css"]
# Theme
THEME = "themes/pelican-alchemy/alchemy"

# Time
TIMEZONE = "Europe/Ljubljana"

DEFAULT_LANG = "en"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ("Gitlab", "https://gitlab.com/brodul"),
    ("Github", "https://github.com/brodul"),
    ("LinkedIn", "https://www.linkedin.com/in/brodul/"),
)

# Social widget
SOCIAL = (
    ("twitter", "http://twitter.com/brodul"),
    ("github", "http://github.com/brodul"),
)

DEFAULT_PAGINATION = 3

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

# GitHub pages

STATIC_PATHS = ["extra/CNAME", "extra/favicons"]
EXTRA_PATH_METADATA = {
    "extra/CNAME": {"path": "CNAME"},
    "extra/favicons/": {"path": ""},
}


DISQUS_SITENAME = "brodul"

# Custom pages
TEMPLATE_PAGES = {"custom/devopsdays.html": "devopsdays.html"}
ARTICLE_EXCLUDES = ["custom"]
