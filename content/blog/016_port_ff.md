Title: Free and powerful alternative to ngrok
Date: 2018-01-22 17:08
Modified: 2018-02-05 12:45
Category: Tips
Tags: ssh, tips


In the days that ngrok did not exists. We used a simple ssh protocol feature called port forwarding.
SSH port forwarding enables us to tunnel an application to the remote server or vice versa.

To do this you will need:

  - a UNIX like (e.g.) linux, with nginx
  - a domain (A record should point to e.g. `demo.brodul.org`)


In our example we will use nginx. In `/etc/nginx/sites-enabled` create a new file `demo.brodul.org.conf`.

```
server {
    server_name demo.brodul.org;

    location / {
      proxy_pass http://127.0.0.1:4000;
    }
}
```

If the browser request has the `Host` header demo.brodul.org it will pass the requests to the service that is listening on localhost port 4000.
We don't have that service yet, we will do that with port forwarding.

After the configuration change we need to reload the nginx server with: `service nginx reload`

On the developer machine we will port forward a development server runing on port 8000 to the remote server.

`ssh -L 4000:localhost:8000 demo.brodul.org`

Let us explain the command above:

 - `-L` we want to forward a local connection
 - `4000:` to port 4000
 - `localhost:8000` which connection to forward
 - `demo.brodul.org` which server to connect to

You will get a shell from the remote server. As long this connection is active you will be able to access the application.

If you have any questions or suggestions contact me on [twitter @brodul](https://twitter.com/brodul)
