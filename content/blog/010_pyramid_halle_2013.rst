Pyramid sprint in Halle
=======================

:date: 2013-08-30 19:09
:tags: pyramid, pylons
:category: GSoC

I was in Halle on a Pyramid/Pylons sprint which was organized by GoCept
company.

Featuring a lot of European hackers. You can read the report here _ 
So we were mainly hacking on the Pyramid, Colander, Deform. And also some other
stuff like pytest and custom logging for applications. 

Also Chris McDonough was in Halle so we were chatting.

He made some suggestion about the GUI for pyramid_debugtoolbar.

- The toolbar has a lot global informations (Versions, Settings, Tweens
  and Introspection panels) so we will have 3 tabs
  (History, Global information and Settings).

- The "History" tab will have the current view

- The "Global informations" will have the setting that all requests share with
  each other. 

- The "Setting" tab will have the Panels settings (the performance panel needs
  to be activated and so)

Pyramid debug toolbar
---------------------

Things I fixed:
+++++++++++++++

- We have localization support in the back-end

- We escaped the variables passed to the Mako templates (because MarkSafe does
  not support py3.2)

- Some smaller things

Thing other people fixed:
+++++++++++++++++++++++++

Big shoutout for adroullier who added the option to disable the injection of
HTML in your application.

https://github.com/Pylons/pyramid/wiki/Pyramid-and-Pylons-Project-Summer-Sprint-2013-Wrap-Up



