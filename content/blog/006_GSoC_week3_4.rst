GSoC - Week 3 and 4
===================

:date: 2013-07-16 10:29
:tags: pyramid, pylons
:category: GSoC

Things done so far
++++++++++++++++++

So I fixed the problem when exception is encoded in something different than ASCII (`#108 <https://github.com/Pylons/pyramid_debugtoolbar/pull/108>`_ ).

I located some dead code, before I make the pull request I would like to run selenium tests but they fail because of the Firefox 22 API upgrade. So currently Selenium RC (2.33.0) does't work with the latest firefox. 
There is a console mode in the current pyramid_debugtoolbar and it's not used. I noticed that when I found a `typo <https://github.com/Pylons/pyramid_debugtoolbar/commit/437033d914ab40804fc85f117265adb4714afb88#L1R18>`_ in the javascript and inspected the CONSOLE_MODE variable that is set in the `view <https://github.com/Pylons/pyramid_debugtoolbar/blob/master/pyramid_debugtoolbar/views.py#L98>`_
Thing that will be fixed soon:
`#112 <https://github.com/Pylons/pyramid_debugtoolbar/issues/112>`_ is very simple but the localization event hooks are not called in tests.
So I need to fix that.

Chris Mcdonough and Chris Rossi are working on the separation of debug toolbar into iframe.
See branch `breckprog-refactor <https://github.com/Pylons/pyramid_debugtoolbar/tree/breckprog-refactor>`_
I looked at the code. Now the debug_toolbar is a separate wsgi app. Which solves a lot of issues.
Like `the issue #99 <https://github.com/Pylons/pyramid_debugtoolbar/issues/99>`_ where the before render event is called multiple times. 


Things I learned:
+++++++++++++++++

I really liked the  _ = lambda x: x
Wrap string in a "_" function makes translations simpler later on.

Selenium is great but if you have issues look at the issue tracker, sometimes the browser change the API and the selenium breaks.

Debugging tools are complex, they need to wrap a lot of things, this is hard to do in a framework, even harder without one.

What's up next:
+++++++++++++++

- I will try to help on the https://github.com/Pylons/pyramid_debugtoolbar/tree/breckprog-refactor branch. 

- Implement localization. 
