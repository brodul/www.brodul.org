GSoC - 5-6
==========

:date: 2013-08-01 12:58
:tags: pyramid, pylons
:category: GSoC


Things done so far
++++++++++++++++++

In the week 5: I fixed the old frontend in the `breckprog-refactor branch <https://github.com/Pylons/pyramid_debugtoolbar/tree/breckprog-refactor>`_, so the branch is usable.

I was mainly talking to Chris Rossi and Blaise Laflamme.

In the week 6 I attended the Kotti and NixOS sprint.
Kotti is a CMS build on top of pyramid (traversal and SQLAlchemy).

Blaise Laflamme said that we need a more simplistic frontend 
with some basic bootstrap. The side affect is that we don't need that much custom js/css code.

We were debating about which requests to represent in the view.
So the last 10 would be nice. With a tag on those which raise an exception.

So I did some progress with the frontend in breckprog-refactor  branch. So the template now changed. You can test it with the demo app provided with the pyramid debug toolbar. 

So now we have a column based view: | Requests | Panels | Panel content |
That enables the user to switch trough the requests, so inspecting responses in the browser to get the requests id is no longer necessary. This will be nice when we will have an API and some js lib like backbone, so we can pull the new requests. So basically like Chromium or Firefox developer tools but with the information from the panels.

**Update:**
Please see this `branch <https://github.com/brodul/pyramid_debugtoolbar/tree/bootstrap-gui>`_

Things I learned:
+++++++++++++++++

I really enjoyed the `sprint <http://www.coactivate.org/projects/zidanca-sprint-2013/project-home>`_
Talking to smart people makes you learn new things, it's hard to tell what I lerned there are so many thing that we were talking about. 

Editing the Mako templates, bootstrap and jQuery things and the minor backend changes were straight forward tasks.


What's up next:
+++++++++++++++

- I will make my changes mergable till Sunday.

- Change the LRUcache to FIFO Queue.
