GSoC - 11-12
============


:date: 2013-09-12 12:58
:tags: pyramid, pylons
:category: GSoC

Please chechout the bootstrap-gui repository at https://github.com/brodul/pyramid_debugtoolbar/tree/bootstrap-gui .
Feel free to ping me @ #pyramid IRC (@Freenode) channel.


To check my work follow this steps:

- `rm -r VIRT_ENV_DIR/lib/python2.7/site-packages/pyramid_debugtoolbar*`
- `cd your_project`
- `mkdir src`
- `cd src`
- `git clone https://github.com/brodul/pyramid_debugtoolbar.git`
- `cd pyramid_debugtoolbar`
- `git checkout bootstrap-gui`
- `VIRT_ENV/python setup.py develop`
- `cd your_project`
- `VIRT_ENV/python setup.py develop`

If you use buildout take a look at mr.developer

Things done so far
++++++++++++++++++

- I introduced the tabs to the UI

- Moved the enable setting from the panels list to a seperate tab

- Moved the global panels to a seperate tab

- The toolbar object from the static requests aren't stored

- Fix the UI for SQLA Select and Examine views (now loads into modals)


Things I learned
++++++++++++++++

- Bootstrap 2 and Bootstrap 3 introduce some unnecessary migration work


What's up next:
+++++++++++++++

- I will wrap all the strings for translation (https://github.com/brodul/pyramid_debugtoolbar/tree/i18n )

