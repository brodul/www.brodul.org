Title: 101 Anomaly detection with Prometheus
Slug: anomaly_detection_1
Date: 2020-08-06 15:46
Category: Tips
Tags: prometheus, monitoring, grafana
Status: draft

## Intro



The work is based on the work done by great engineers at GitLab. Check out their blog post ["How to use Prometheus for anomaly detection in GitLab - Sara Kassabian"](https://about.gitlab.com/blog/2019/07/23/anomaly-detection-using-prometheus/) and ["Monitorama PDX 2019 talk - Practical Anomaly Detection using Prometheus - Andrew Newdigate"](https://vimeo.com/341141334).

The techniques presented there are great. Everyone should look at graphs from time to time, to see if something strange is going on.
But the reallity is that we need other things fixed or shipped. This blog series is designed as a getting started guide. I will try to explain concepts and provide snippets so you can quickly create a new grafana panel and see if the techniques work for you.

I would recommend that you do anomaly detection on the metrics or indicators that are important for your [Service Level Objectives (SLO)](https://landing.google.com/sre/sre-book/chapters/service-level-objectives/). For the start we will take a look at indicators that show some seasonality. For example system system throughput, typically measured in requests per second, is changing trough the week. You will see a pattern repeating it self every week.

At first we will not optimize the prometheus queries with `record` configuration. So you should first try the concepts of this blog post and read another blog post in the series to optimize the queries.

<!-- ![Query configuration]({static}/images/anomaly_detection_1/1_varaible.jpg) -->

In a later blog post we might take a look at a indicator that does not have seasonability but a normal/gausian distribution. An example of that would be the requests latency of a specific end point.

<!-- ![Traffic]({static}/images/anomaly_detection_1/2.jpg) -->

## The metric

I mentioned above, that you can use this technique to detect anomalies in system throughput. Requests per second in a system are similar to traffic counters on highways. So I will use a traffic counter on the highway to the city. As you can imagine we have a morning rush hour, so the metric is a bit more demanding than traditional requests per seconds to a site. Another thing to consider is that the dataset is in holiday season so we will have to take that into acount. One week the amount of cars is lower that another, because people are not commuting into the city, enjoying their vacation.

I will use Grafana `$q` dashboard query variable in all my queries in Grafana panels. So it's easier for you to copy/paste the examples and play around:

Let's take a quick look at the traffic. The counter is a sliding window counter. It return the number of vaciacles in last hour and is pulled every 5 minutes:

## Conclusion
