GSoC - 13-14
============

:date: 2013-09-19 14:28
:tags: pyramid, pylons
:category: GSoC

Things done so far
++++++++++++++++++

- Migrated from bootstrap 2.3.2 to bootstrap 3.

- Design changes:

  - Removed death space in Global panel
  - Fixed tables styling 
  - Made panels to horizontal navbars
  - The current selected request is represented clearly


Things I learned
++++++++++++++++

- If you do migration from bootstrap 2 to 3 be careful if you use modals
  with external content. 
  (In bootstrap 3 the whole modal needs to
  be defined in the external source not just the body)

- It seems that the old selenium drivers are useless so we need to use the new
  ones.

What's up next:
+++++++++++++++

The pencil down was already called but I will continue to work on the pyramid debug toolbar.

The reasons are simple: my work is currently not mergable I have to fix the selenium test,
add some unit tests and polish the GUI a bit more.
Then release an alfa and poke the experienced people to give bugreports and
then when mcdonc approves it release with the new pyramid.

I think only then the GSoC work makes sense.

I would like to thank you all I learned a lot.
