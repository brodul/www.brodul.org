GSoC - Week 0 - Intro
=====================

:date: 2013-06-16 16:29
:tags: pyramid, pylons
:category: GSoC

Big surprise
++++++++++++

I have been programming in python for the last 2 years,
it all started with replacing Bash scripts with some simple python scripts.

The scripts became more and more complex and I started enjoying programming in general.
I also realized that the demand of standard system administrators is decreasing 
and webdev seemed to be a good solution. So I looked into Plone, Django and BFG/pylons (now pyramid).

Then I had a opportunity to work on some small pyramid projects. And while learning the pyramid and other thing I
did some small commits to the Shootout projects.

After talking to Domen Kožar I applied to GSoC. And I have been excepted.

About the project
+++++++++++++++++

I haven't super JavaScript experience so I have read 'JavaScript the good parts' and I still reading the project documentation and code. So I am behind my time line. So I decided I will write another blog post then I see the big picture.

Communication
+++++++++++++

After talking to Domen I decided I will write at least one blog post a week. This will help me work harder and evaluate stuff I have done in the week also setting goals for the next week.

My nickname is brodul and you can find me on FreeNode, twitter.
I am reading the pylons-dev mailing list. I live in a CEST time zone (UTC +2).


Tasks for next week:
++++++++++++++++++++

- Take an exam on Wednesday (Radio-Communications)
- Talk to the mentor (via Skype/Mumble/other)
- Setup the development/test environment on Thursday (if necessary)
- Evaluation of bugs on Github and fixing the easy ones (if present)
