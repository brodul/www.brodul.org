GSoC - Week 1 and 2
===================

:date: 2013-07-02 10:29
:tags: pyramid, pylons
:category: GSoC

Week one and two is over. I finished my exams (in week 1) successfully.

Things done sofar
+++++++++++++++++

I managed to talk to my mentor John Anderson 'sontek'.
So the main priority of my GSoC would be to implement the 2 things in the GSoC:

- Separate the toolbar so it doesn't embed itsequitelf into the site. (primary goal)
- Setup the debugger inside the pyramid debug toolbar


I was merging pull requests previous week.
`#83 <https://github.com/Pylons/pyramid_debugtoolbar/pull/83>`_ and `#43 <https://github.com/Pylons/pyramid_debugtoolbar/pull/43>`_ will be resolved shortly.

Regarding pull request #43, SQL systems can support multiple Character Set. Please take a look at the PostgreSQL Character Set Table:
http://www.postgresql.org/docs/8.1/static/multibyte.html#CHARSET-TABLE

Therefore a setting for the encoding of the Database exceptions might be useful, with some sane defaults (like utf-8). The other option is that the community decides that there are just some codecs supported like 'utf-8' and 'latin-1' which I think is a bad idea and not compliant with the zen of Pyramid.

**Update:**
Chris Mcdonough, suggested to encode the byte string with the 'replace' handling. 

Things I learned:
+++++++++++++++++

Tox is very useful when you want to support multiple version of python and there is absolutely no reason not to use it (even if you use travis-ci).  If you have 6 minutes please consider to listen to this talk by `Corey Goldberg <http://www.youtube.com/watch?v=Oldkj519o4A>`_

Python 3.2 is so much pain. Writing python 2 code that is portable to python 3.2 is problematic. But please don't drop python 3.2.  The problem is that the current stable and testing Debian version use 3.2 as the default python3.

Unicode, character sets and codecs are things you must understand and handle in you program. People don't speak English and you can not change them. So learn how to handle unicode in you program if you haven't done this already. Please see this `talk <http://pyvideo.org/video/948/pragmatic-unicode-or-how-do-i-stop-the-pain>`_ . If you understand how to deal with unicode encoding and decoding, the talk may be helpful when explaining unicode to newbies. 

What seem's to be the problem officer
+++++++++++++++++++++++++++++++++++++

I don't have useful experience in Python 3 so far, so am a bit careful when debugging test for Python3.

The codebase is quite complex, which means I learn a lot of new stuff. But I can't resolve the issues as fast as I would wish.

So the best way to  solve this issue is to work more hours, as by now my brain adapted to the normal work cycle.

What's up next
++++++++++++++

- Dive in into issues
- Totally understand the code base
- Research the solutions for separating the toolbar from the site (iframe or js inject)
- Start implementing it

Do this in parallel.
:D
