Title: About me

# Andraž 'brodul' Brodnik

SUMMARY
=======

Seasoned Senior Software Architect and Site Reliability Engineer with
10+ years of experience in Python, JavaScript, and Bash who also worked
with Elm, Nix, Elixir with experience designing, implementing, and
managing cutting-edge deployment automation of cloud resources.

SKILLS
======

-   Python (Flask, Pyramid, Django), Elm, JavaScript (Node, Vue, React...), Nix, Bash, Elixir
-   Docker, Kubernetes, Ansible, Terraform, Prometheus/Grafana, CI/CD
-   Redis, Apache Cassandra, Psql
-   GCP (GKE, GCE, GCS, DataFlow, BQ, SQL ... ) and AWS (EC2, S3, Route53, SQS, SES, IAM RDS)

EXPERIENCE
==========

-   [GLOBALWEBINDEX](https://www.globalwebindex.com/), Senior Software Engineer   Feb 2019 -- May 2020 (contract)

    -   Create and maintain fully automated CI/CD pipelines for code
        deployment using CircleCI, Helm and Kubernetes
    -   Solve architectural challenges and keep the platform stable and
        meet the SLOs
    -   Build stress tests and design auto scaling to prepare the
        platform for global launch
    -   Adapt monitoring to measure what is valuable for customers and
        the product team
    -   Actively manage, improve, and monitor cloud infrastructure on
        GCP, GCE, GKE, DataFlow, Cloud SQL, including backups, patches,
        and scaling
    -   Maintain Apache Cassandra cluster and Redis HA to enable low
        latency big data business decisions
    -   Transform tribal knowledge to infrastructure as a code with
        Terraform and reduce cost

-   [BRODUL S.P.](https://brodul.org/), Consultant  Dec 2015 - Present

    -   Manage/coach teams and team leads
    -   Offer professional services and training on
        concepts/technologies like Cloud infrastructure, OKRs and
        planning, Disaster Recovery, security, Kubernetes, Prometheus,
        Python, Git
    -   Build ETL systems, hardware testing framework, interconnection
        with robots (OPC-UA)

-   [RECIPROCITY INC. ](https://reciprocitylabs.com/), Software engineer  Dec 2015 -- Oct 2017 (contract)

    -   Responsible for delivering features on time and fix bugs
        (Python/JS/Golang)
    -   Maintain critical infrastructure and manage 20+ RDS and AWS EC2
        instances. Create and maintain fully automated CI/CD pipelines
        for code deployment using Jenkins
    -   Working closely with the product team, writing
        technical/infrastructure documentation, implement the feature.
        cover with unit, integration and E2E tests
    -   Responsible for SOC2 compliance process
    -   Core security team member

-   APPOTEKA, System Administration/Python Developer Oct 2013 -- Feb 2015
-   Niteo d.o.o. , System Administration/Python Developer May 2011 -- Oct 2011
-   Nil Ltd. , Embedded System Developer May 2011 -- Oct 2011
-   Zemanta, System Administration Intern Jun 2010 -- Aug 2010

CERTIFICATES
============

![_](https://api.accredible.com/v1/frontend/credential_website_embed_image/badge/20327828)

[Google Cloud Certified - Professional Cloud Architect (July 7, 2020 - July 7, 2022)](www.credential.net/6b36aebf-7435-4395-b8dd-243fae770a1a)


VOLUNTEER EXPERIENCE
====================

-   [Organizer -- Bsides Ljubljana](https://bsidesljubljana.si/) (2016 -
    2020)
-   President -- Linux User Group Slovenia (2016 - 2020)
-   Member -- Kiberpipa hackerspace (2008 -2014 )
-   Mentor -- Django Girls Ljubljana
-   Program Committee member -- PyConLT (2019)

PERSONAL PROJECTS
=================

-   Security and python talks (OSSEC, MITM proxy, idiomatic python ...)
-   Blog [brodul.org](https://www.brodul.org/)
-   Code:
    -   [Personal infrastructure](https://github.com/brodul/brodul-infra) (HCL)
    -   [Password generator](https://gitlab.com/brodul/diceslo) (Elm)
    -   [Configuration Object](https://repl.it/@brodul/ConfiguratorMadness) (Python)

EDUCATION
=========

[University of Ljubljana](https://www.uni-lj.si/eng/)

[Bachelor of Science (BS), Electrical engineering, Telecommunications   2008 -- 2016](http://www.fe.uni-lj.si/en/)

